#!/usr/bin/env bash
#
### Program Name: Borgbackup
#
### Program Author: DreamTeam
#
### Program Version:
#
### Program Purpose:
# Borgbackup is a wrapper script for a backup utility called Borg
#
################################################################################

# This section is for toggling debugging on/off
# "set -x" toggles debugging on
# "set +x" toggles debugging off
set +x;

# Loads the program's libraries
# They must be in "../lib" relative borgbackup's "./bin" directory
borgbackup_script_path="$(readlink -f "${BASH_SOURCE[0]}" 2>/dev/null||echo $0)";
borgbackup_source_dir="$(dirname ${borgbackup_script_path})";
borgbackup_lib_dir="${borgbackup_source_dir%/bin}/lib/";
source "${borgbackup_lib_dir}/util-libs.sh";
for utilfunction in \
                    InputSanitize \
                    utilDeps \
                    utilLog \
                    CheckIfEmpty \
                    parse_yaml \
                    ;
do
    readonly -f "${utilfunction}";
done;

# check for dependencies
dep_pkgs=( awk sed )
utilDeps dep_pkgs

utilLog /var/log/borgbackup \
  || { \
        exit 1;
};


#Load yaml configuration file from /etc/borgbackup and parse values;
LoadYamlConfig(){
if [ -f "/etc/borgbackup/borgbackup_runner_config.yml" ]; then
#read yaml file
eval "$(parse_yaml /etc/borgbackup/borgbackup_runner_config.yml "config_")";

#set variables from values in config file
readonly username="$config_connection_username";
readonly ansible_user="$config_connection_username";
readonly server="$config_connection_backup_server";
readonly client_hostname="$config_connection_client_hostname";
readonly job_queue_path="$config_connection_job_queue_path";
readonly ssh_key_file="$config_connection_ssh_key_file";
readonly repo_key="$config_connection_repo_key";
readonly repository="$config_connection_backup_repository";
readonly keep_daily="$config_retention_keep_daily";
readonly keep_weekly="$config_retention_keep_weekly";
readonly keep_monthly="$config_retention_keep_monthly";

for dir in "${config_backup_locations[*]}"; do
        dir_list="$dir";
done;

for dir in "${config_backup_locations[*]}"; do
        echo "$dir";
done;

else
  echo "missing configuration file" > >(tee -a ${_r_log_dir}/${_r_datestamp}_backup_client_${_r_run_id}.error.log >&2);
fi
}

BackupJobName(){
echo ${HOSTNAME}-$timestamp
}



PreBackupCommands(){
  # Run additional backup steps stated in config file
  if [ ! -z "$config_extras_run_command" ]; then
    LogInfo "Pre run command not specified. nothing to do here"
    LogInfo "Pre run command specified. running prerun command"
    $config_extras_run_command
    if [ "$?" -gt 0 ]; then
      echo "Pre run command failed"  > >(tee -a ${_r_log_dir}/${_r_datestamp}_backup_client_${_r_run_id}.error.log >&2);
      exit
    fi
  fi
}

PerformBackup(){
   #Backup directories using borg
   echo "running borg backup"  > >(tee -a ${_r_log_dir}/${_r_datestamp}_backup_client_${_r_run_id}.event.log >&1);
   # set environment variables for borg
   export BORG_RSH="ssh -i $ssh_key_file"
   export BORG_PASSPHRASE="$(cat $repo_key)"

   borg create -v --stats --progress --compression=lz4 --one-file-system \
	   ${username}@${server}:${repository}/${client_hostname}::"$(BackupJobName)" \
       $dir_list &> >(tee -a ${_r_log_dir}/${_r_datestamp}_backup_client_${_r_run_id}.event.log >&1)
   if [ "$?" -gt 0 ]; then
     echo "Borg backup failed" > >(tee -a ${_r_log_dir}/${_r_datestamp}_backup_client_${_r_run_id}.error.log >&2);
     exit
   fi
}

RotateBackups(){
  #Prune backups using borg
  borg prune -v --list ${username}@${server}:${repository}/${client_hostname} --prefix ${HOSTNAME}- \
    --keep-daily=$keep_daily --keep-weekly=$keep_weekly --keep-monthly=$keep_monthly \
  &> >(tee -a ${_r_log_dir}/${_r_datestamp}_backup_client_${_r_run_id}.event.log >&1)
}

GenerateJobFile(){
  #create job file on backup server
  return_code="$?"
  if [[ -z ${config_postrun_executor}  ]]; then
    echo "post run command  not specified. nothing to do here" > >(tee -a ${_r_log_dir}/${_r_datestamp}_backup_client_${_r_run_id}.event.log >&1)
  else
    #cat file via ssh to remote server
    ssh -i $ssh_key_file  ${username}@${server} "cat > /${job_queue_path}/job_$(BackupJobName)" <<EOF
name: $(BackupJobName)
backup_status: $return_code
web_hook: "${config_web_hook}"
post_run_executor: ${config_postrun_executor}
ansible_user: ${config_postrun_ansible_user}
playbook: ${config_postrun_playbook}
role_requirements:
$(printf "%s\n" "${config_postrun_role_requirements[@]/#/  - }")
EOF
  fi
}

main() {
  timestamp=$(date +%Y-%m-%d_%H-%M-%S)
  LoadYamlConfig
  PreBackupCommands
  PerformBackup
  RotateBackups
  GenerateJobFile
}

main
