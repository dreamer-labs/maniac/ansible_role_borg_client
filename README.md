Role Name
=========

ansible_borg_client

This role will setup a borg and a wrapper script borgbackup_runner on a client.

What is Borg?
BorgBackup is a deduplicating backup program. Optionally, it supports compression and authenticated encryption. See [BorgBackup Docs](https://borgbackup.readthedocs.io/en/stable/)

What is borgbackup_runner?
In short, the borgbackup_runner initiates a backup of user defined local directories. After a successful backup, the borgbackup_runner will submit a user defined post job to the backup repo. Every minute the postjob_runner will check the queue for post job request and execute it. This is useful for testing backups.

Additionally, borgbackup_runner has the ability to run a custom command prior to running the backup job. This is useful when backing up databases using a 3rd party utility like Holland. See Example playbook below on how to use this.

Requirements
------------

git, awk, and sed must be installed

Role Variables
--------------

```
borg_client_required_packages:
  yum:
    - borgbackup
borg_client_backup_runner_log_directory: /var/log/borgbackup # where to store logs
borg_client_backup_user: borg # the user account borg should run under
borg_client_borg_repo_path: /backups # where to store client backup repos
borg_client_borgrunner_cron_daily: 23 # specify what time of day to run a backup
borg_client_borgrunner_pre_run_command: sudo holland bk # specify a command to run on the client  before a backup job runs
borg_client_borgbackuprunner_install_path: /usr/local/borgbackup # specify where to install the borg backup runner
borg_client_borgbackuprunner_config_name: borgbackup_runner_config.yml # the name of the borgbackup runner configuration file
borg_client_postjob_runner_executor: ansible # specify the type of executor to use for post backup jobs
borg_client_home_directory: /home/borg # the location of the borgbackup runner's home directory
borg_client_borg_repo_ssh_host: backup_repo # the hostname of the borg repo server
borg_client_hostname: client1 # the hostname of the borgbackup client.
backup_set: "{{ lookup('env','BACKUP') }}" # used by the borgbackup post job runner
file_to_restore: var/spool/holland/default/newest/backup_data/test.sql.gz # specify a file or directory to restore from backup
restore_path: /tmp/restore # specify where the files should be restored to
borg_client_postjob_runner_queue_path: /backups/postjobs # specify where to store the post backup job request.
borg_client_retention_policy: # specify the job retention policy.
  keep_hourly: 3
  keep_daily: 7
  keep_weekly: 4
  keep_monthly: 6

ansible_executor: # define a post backup job. This is optional
  playbook_source: git+https://gitlab.com/dreamer-labs/maniac/ansible_test_dr.git # specify the location of the playbook to run after a successful backup
  role_requirements: # specify where to download the roles required by the playbook
    - git+https://gitlab.com/dreamer-labs/maniac/ansible_role_borg_client.git
    - git+https://gitlab.com/dreamer-labs/maniac/ansible_role_mariadb.git
  ansible_username: "{{ borg_client_backup_user }"

borg_client_borg_repo_ssh_priv_key: | # specify the private ssh key to be used by borg

borg_client_borg_repo_ssh_pub_key: | # specify the public ssh key to be used by borg
borg_client_backup_user_pass: | # specify a passphrase to use to encrypt the borg repo.
    xxxxxxxxxxxxxxx
```

Dependencies
------------

ansible_role_packages
ansible_role_repository

Example Playbook
----------------

```
- hosts: borgclients
  vars:
    borg_client_borgrunner_pre_run_command: sudo holland bk
  roles:
     -  role: ansible_role_borg_client
```

License
-------

BSD

Author Information
------------------

The Development Range Engineering, Architecture, and Modernization (DREAM) Team
